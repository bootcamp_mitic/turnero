from django import forms
from . import models
#from django.contrib.auth.models import User
from django.contrib.auth import get_user_model

# Se importa el modelo de usuarios
Users = get_user_model()

#formulario personalizado para personas en las vistas de usuario
# Se hace una manha para que sea requerido el campo, pero no rompa las bolas con la validacion al ingresar
# directamente con widgets y atributos del mismo
class PersonasForm(forms.Form):
    nombre = forms.CharField(max_length=100, required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))
    apellido = forms.CharField( max_length=100, required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))
    documento = forms.CharField(max_length=10, required=False)
    telefono = forms.CharField(max_length=50, required=False)
    email = forms.EmailField(required=False)
    estado = forms.ModelChoiceField(queryset=models.Estados.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))

# formulario personalizado para estados en las vistas de usuario
class EstadosForm(forms.Form):
    estado=forms.CharField(max_length=80, required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))
    codigo=forms.IntegerField(required=False,
                           widget= forms.NumberInput
                           (attrs={
                               'required': 'True'
                            }))

# formulario personalizado para tipocliente en las vistas de usuario
class TipoClienteForm(forms.Form):
    tipo_cliente=forms.CharField(max_length=70, required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))

# formulario personalizado para clientes en las vistas de usuario
class ClientesForm(forms.Form):
    persona = forms.ModelChoiceField(queryset=models.Personas.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))
    tipo_cliente = forms.ModelChoiceField(queryset=models.TipoCliente.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))
    razon_social=forms.CharField(max_length=100, required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))
    ruc = forms.CharField( max_length=15, required=False)
    estado = forms.ModelChoiceField(queryset=models.Estados.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))

# formulario personalizado para prioridades en las vistas de usuario
class PrioridadesForm(forms.Form):
    descripcion=forms.CharField(max_length=100,required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))
    nivel = forms.IntegerField( required=False,
                           widget= forms.NumberInput
                           (attrs={
                               'required': 'True'
                            }))
    estado = forms.ModelChoiceField(queryset=models.Estados.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))

#formulario personalizado para tickets en las vistas de usuario
class TicketsForm(forms.Form):
    descripcion=forms.CharField(max_length=250, required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))
    inicio_numeracion = forms.IntegerField( required=False,
                           widget= forms.NumberInput
                           (attrs={
                               'required': 'True'
                            }))
    fin_numeracion = forms.IntegerField( required=False,
                           widget= forms.NumberInput
                           (attrs={
                               'required': 'True'
                            }))
    estado = forms.ModelChoiceField(queryset=models.Estados.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))

#formulario personalizado para servicios en las vistas de usuario
class ServiciosForm(forms.Form):
    descripcion=forms.CharField(max_length=100, required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))
    estado = forms.ModelChoiceField(queryset=models.Estados.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))

#formulario personalizado para terminales en las vistas de usuario
class TerminalesForm(forms.Form):
    servicio = forms.ModelChoiceField(queryset=models.Servicios.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))
    usuario = forms.ModelChoiceField(queryset=Users.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))
    descripcion=forms.CharField(max_length=100, required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))
    estado = forms.ModelChoiceField(queryset=models.Estados.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))

#formulario personalizado para colas en las vistas de usuario
class ColasForm(forms.Form):
    terminal = forms.ModelChoiceField(queryset=models.Terminales.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))
    ticket = forms.ModelChoiceField(queryset=models.Tickets.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))
    descripcion=forms.CharField(max_length=150, required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))
    max_personas = forms.IntegerField( required=False)
    incremento = forms.IntegerField( required=False)
    estado = forms.ModelChoiceField(queryset=models.Estados.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))


#formulario personalizado para colasclientes en las vistas de usuario
class ColasClientesForm(forms.Form):
    cliente=forms.CharField( max_length=15, required=False,
                           widget= forms.TextInput
                           (attrs={
                               'required': 'True'
                            }))
    cola = forms.ModelChoiceField(queryset=models.Colas.objects.all(), required=False,
                           widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))
    prioridad = forms.ModelChoiceField(queryset=models.Prioridades.objects.all(), required=False, 
                            widget= forms.Select
                           (attrs={
                               'required': 'True'
                            }))

class LoginForm(forms.Form):
    usuario = forms.CharField(max_length=50, required=True )
    password = forms.CharField(widget=forms.PasswordInput, required=True )
