# Generated by Django 4.1.7 on 2023-03-20 05:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_colas_es'),
    ]

    operations = [
        migrations.RenameField(
            model_name='terminales',
            old_name='usu_id',
            new_name='usu',
        ),
    ]
