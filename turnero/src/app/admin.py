from django.contrib import admin
from . import models
# Register your models here.

@admin.register(models.Estados)
class EstadosAdmin(admin.ModelAdmin):
    list_filter = []
    list_display = ["es_desc", "es_cod"]
    list_editable = []
    list_per_page = 20
    search_fields = ["es_desc", "es_cod"]
    #form = None

@admin.register(models.Personas)
class PersonasAdmin(admin.ModelAdmin):
    list_display = ["per_nombre", "per_apellido", "per_cedula", "es_id"]
    list_filter = []
    list_editable = []
    list_per_page = 20
    search_fields = ["per_nombre", "per_apellido", "per_cedula"]
    #form = None

@admin.register(models.TipoCliente)
class TipoClienteAdmin(admin.ModelAdmin):
    list_display = ["tc_desc"]
    list_filter = ["tc_desc"]
    list_editable = []
    list_per_page = 20
    search_fields = ["tc_desc"]
    #form = None

@admin.register(models.Clientes)
class ClientesAdmin(admin.ModelAdmin):
    list_display = ["cli_razon_social", "cli_ruc", "tc_id", "es_id"]
    list_filter = ["cli_razon_social"]
    list_editable = []
    list_per_page = 20
    search_fields = ["cli_razon_social", "cli_ruc"]
    #form = None

@admin.register(models.Prioridades)
class PrioridadesAdmin(admin.ModelAdmin):
    list_display = ["pri_desc", "es_id"]
    list_filter = ["es_id"]
    list_editable = []
    list_per_page = 20
    search_fields = ["pri_desc"]
    #form = None

@admin.register(models.Tickets)
class TicketsAdmin(admin.ModelAdmin):
    list_display = ["ticket_desc", "ticket_inicio_num", "ticket_fin_num", "es_id"]
    list_filter = []
    list_editable = []
    list_per_page = 20
    search_fields = ["ticket_desc"]
    #form = None

@admin.register(models.Servicios)
class ServiciosAdmin(admin.ModelAdmin):
    list_display = ["ser_desc", "es_id"]
    list_filter = []
    list_editable = []
    list_per_page = 20
    search_fields = ["ser_desc"]
    #form = None

@admin.register(models.Terminales)
class TerminalesAdmin(admin.ModelAdmin):
    list_display = ["ter_desc", "ser_id", "usu_id", "ser_id","es_id"]
    list_filter = []
    list_editable = []
    list_per_page = 20
    search_fields = ["ter_desc", "ser_id"]
    #form = None

@admin.register(models.Colas)
class ColasAdmin(admin.ModelAdmin):
    list_display = ["cola_desc", "ticket_id"]
    list_filter = []
    list_editable = []
    list_per_page = 20
    search_fields = ["cola_desc"]
    #form = None

@admin.register(models.ColasClientes)
class ColasClientesAdmin(admin.ModelAdmin):
    list_display = ["cola_id", "cli_id", "pri_id", "cola_entrada", "cola_salida"]
    list_filter = ["pri_id", "cola_entrada"]
    list_editable = []
    list_per_page = 20
    search_fields = ["cli_id", "cola_id", "pri_id"]
    #form = None