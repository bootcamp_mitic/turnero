from django.shortcuts import render, redirect, get_object_or_404
from . import forms, models
from django.contrib import auth,messages
from django.contrib.contenttypes.models import ContentType
import datetime

# Create your views here.


#Obtener nombre de las tablas usadas en django
menues = ContentType.objects.filter(app_label='app')

def home(request):
    #form = forms.ColasClientesForm(request.POST or None)
    #context = None
    #Boton de salir, para desloguearse
    if request.POST.get('logout') is not None:
        if request.POST['logout'] == 'true':
            auth.logout(request)
            return redirect('login')

    #navegacion entre abms
    #if request.POST.get('model') is not None:
    return redirect_to(request, request.POST['model'] if request.POST.get('model') is not None else None)

    
    # if form.is_valid():
    #     print(form.data)

    # context = {
    #     "titulo": "Formulario",
    #     "da_form": form,
    #     "tabla": cola,
    #     "menues":menues
    # }
    # return render(request, "home.html", context)


def login(request):
    form = forms.LoginForm(request.POST or None)
    if request.user.id is not None:
        print("el id del usuario", request.user.id)
        return home(request)
    if form.is_valid():
        print(form.data)
        user = auth.authenticate(username=form.data.get('usuario'), password=form.data.get('password'))
        print(user)
        if user is not None:
            auth.login(request, user)
            return home(request)
        else:
            messages.error(request, 'Usuario o Contraseña Incorrectos')
            return redirect('login')

    context = {
        "login_form": form
    }
    return render(request, "login.html", context)





# Other methods

def redirect_to(request, model, context=None):
    print(request, model, context)
    if model == 'clientes':
        context_aux = {}
        form = forms.ClientesForm(request.POST or None, initial={})
        if request.POST.get("delete") is not None and request.POST.get("delete") == "true":
            objToDelete = models.Clientes.objects.filter(cli_id=request.POST.get("id"))
            objToDelete.delete()
            context_aux['message']= 'Eliminacion Exitosa'

        if request.POST.get("edit") is not None and request.POST.get("edit") == "true":
            objToMod = get_object_or_404(models.Clientes, cli_id=request.POST.get('id'))
            initialData = {'estado': objToMod.es_id,
                            'persona': objToMod.per_id,
                            'tipo_cliente': objToMod.tc_id,
                            'razon_social': objToMod.cli_razon_social,
                            'ruc': objToMod.cli_ruc
                        }
            context_aux['id']= objToMod.cli_id
            form = forms.ClientesForm(initial=initialData)

        if form.is_valid()and request.POST.get('isForSave') is not None:
            if(request.POST.get('isForSave')=='true'): #Validacion extra para que no guarde si el campo esta vacio, pq al hacer por post
                # cree que debe mandar ya el formulario, y una vez que ya no manda, las validaciones de los campos impediran que se
                # mande sin completar
                obj = models.Clientes()
                if form.data.get("id") is not None and form.data.get("id") != '':
                    obj = models.Clientes.objects.filter(cli_id=form.data.get("id"))
                    obj.update(es_id=form.data.get("estado"),
                                per_id=form.data.get("persona"),
                                tc_id=form.data.get("tipo_cliente"),
                                cli_razon_social=form.data.get("razon_social"),
                                cli_ruc=form.data.get("ruc"),
                                usuario_mod=request.user.id
                            )
                    context_aux['message']= 'Actualizacion Exitosa'
                else:
                    obj.usuario_alta = request.user.id
                    obj.per_id= form.data.get("persona")
                    obj.tc_id = form.data.get("tipo_cliente")
                    obj.cli_razon_social = form.data.get("razon_social")
                    obj.cli_ruc = form.data.get("ruc")
                    obj.es_id = form.data.get("estado")
                    obj.save()
                    context_aux['message']= 'Insercion Exitosa'
                form = forms.ClientesForm(request.GET or None) 
        if context is None:
            context = {
                "titulo": model,
                "da_form": form,
                "tabla_datos":models.Clientes.objects.raw('''
                SELECT 
                    cli.cli_id,
                    p.per_nombre || ' ' || p.per_apellido AS per_nombre,
                    tc.tc_desc,
                    e.es_desc,
                    cli.cli_razon_social,
                    cli.cli_ruc
                FROM
                    clientes cli, personas p, estados e, tipo_cliente tc
                WHERE 
                    cli.per_id = p.per_id
                    AND e.es_id = cli.es_id
                    AND cli.tc_id = tc.tc_id
                ORDER BY
                    cli.cli_id desc       
                ''')[:10],
                "tabla_titulos": models.Clientes._meta.fields[:6],
                "menues": menues
            }
        return render(request, 'clientes.html', (context | context_aux))
    if model == 'colas':
        context_aux = {}
        form = forms.ColasForm(request.POST or None)
        if request.POST.get("delete") is not None and request.POST.get("delete") == "true":
            objToDelete = models.Colas.objects.filter(cola_id=request.POST.get("id"))
            objToDelete.delete()
            context_aux['message']= 'Eliminacion Exitosa'

        if request.POST.get("edit") is not None and request.POST.get("edit") == "true":
            objToMod = get_object_or_404(models.Colas, cola_id=request.POST.get('id'))
            initialData = {'terminal': objToMod.ter_id,
                            'ticket': objToMod.ticket_id,
                            'descripcion': objToMod.cola_desc,
                            'max_personas': objToMod.cola_max_persona,
                            'incremento': objToMod.cola_increment,
                            'estado': objToMod.es_id
                        }
            context_aux['id']= objToMod.cola_id
            form = forms.ColasForm(initial=initialData)

        if form.is_valid()and request.POST.get('isForSave') is not None:
            if(request.POST.get('isForSave')=='true'): #Validacion extra para que no guarde si el campo esta vacio, pq al hacer por post
                # cree que debe mandar ya el formulario, y una vez que ya no manda, las validaciones de los campos impediran que se
                # mande sin completar
                obj = models.Colas()
                if form.data.get("id") is not None and form.data.get("id") != '':
                    obj = models.Colas.objects.filter(cola_id=form.data.get("id"))
                    obj.update(es_id=form.data.get("estado"),
                                ter_id=form.data.get("terminal"),
                                cola_desc=form.data.get("descripcion"),
                                cola_max_persona=form.data.get("max_personas"),
                                cola_increment=form.data.get("incremento"),
                                ticket_id=form.data.get("ticket")
                            )
                    context_aux['message']= 'Actualizacion Exitosa'
                else:
                    obj.cola_desc = form.data.get("descripcion")
                    obj.cola_max_persona = form.data.get("max_personas")
                    obj.cola_current_persona = 0
                    obj.cola_increment = form.data.get("incremento")
                    obj.es_id = form.data.get("estado")
                    obj.ter_id = form.data.get("terminal")
                    obj.ticket_id = form.data.get("ticket")
                    obj.usuario_alta = request.user.id
                    obj.save()
                    context_aux['message']= 'Insercion Exitosa'
                form = forms.ColasForm(request.GET or None) 
        if context is None:
            context = {
                "request": request,
                "titulo": model,
                "da_form": form,
                "tabla_datos":models.Colas.objects.raw('''
                    SELECT 
                        c.cola_id,
                        t.ticket_desc,
                        e.es_desc,
                        ter.ter_desc,
                        c.cola_desc,
                        c.cola_max_persona,
                        c.cola_current_persona
                    FROM 
                        colas c, tickets t, terminales ter, estados e
                    WHERE
                        c.ticket_id = t.ticket_id
                        AND c.ter_id = ter.ter_id
                        AND c.es_id = e.es_id
                    ORDER BY
                        c.cola_id desc
                ''')[:10],
                "tabla_titulos": models.Colas._meta.fields[:7],
                "menues": menues
            }
        return render(request, 'colas.html', (context | context_aux))
    if model == 'estados':
        context_aux = {}
        form = forms.EstadosForm(request.POST or None, initial={})
        if request.POST.get("delete") is not None and request.POST.get("delete") == "true":
            objToDelete = models.Estados.objects.filter(es_id=request.POST.get("id"))
            objToDelete.delete()
            context_aux['message']= 'Eliminacion Exitosa'

        if request.POST.get("edit") is not None and request.POST.get("edit") == "true":
            objToMod = get_object_or_404(models.Estados, es_id=request.POST.get('id'))
            initialData = {'estado': objToMod.es_desc, 'codigo': objToMod.es_cod }
            context_aux['id']= objToMod.es_id
            form = forms.EstadosForm(initial=initialData)
            
        if form.is_valid()and request.POST.get('isForSave') is not None:
            if(request.POST.get('isForSave')=='true'): #Validacion extra para que no guarde si el campo esta vacio, pq al hacer por post
                # cree que debe mandar ya el formulario, y una vez que ya no manda, las validaciones de los campos impediran que se
                # mande sin completar
                obj = models.Estados()
                if form.data.get("id") is not None and form.data.get("id") != '':
                    obj = models.Estados.objects.filter(es_id=form.data.get("id"))
                    obj.update(es_desc=form.data.get("estado"), es_cod=form.data.get("codigo"))
                    context_aux['message']= 'Actualizacion Exitosa'
                else:
                    obj.es_desc = form.data.get("estado")
                    obj.es_cod = form.data.get("codigo")
                    obj.save()
                    context_aux['message']= 'Insercion Exitosa'
                form = forms.EstadosForm(request.GET or None) 
        if context is None:
            context = {
                "titulo": model,
                "da_form": form,
                "tabla_datos":models.Estados.objects.all().order_by('es_id')[:10],
                "tabla_titulos": models.Estados._meta.fields[:3],
                "menues": menues
            }
        return render(request, 'estados.html', (context | context_aux) ) 
    if model == 'personas':
        context_aux = {}
        form = forms.PersonasForm(request.POST or None)
        if request.POST.get("delete") is not None and request.POST.get("delete") == "true":
            objToDelete = models.Personas.objects.filter(per_id=request.POST.get("id"))
            objToDelete.delete()# A futuro ver si conviene realmente eliminar el registro
            context_aux['message']= 'Eliminacion Exitosa'

        if request.POST.get("edit") is not None and request.POST.get("edit") == "true":
            objToMod = get_object_or_404(models.Personas, per_id=request.POST.get('id'))
            initialData = {'nombre': objToMod.per_nombre, 
                            'apellido': objToMod.per_apellido,
                            'documento': objToMod.per_cedula, 
                            'telefono': objToMod.per_telefono,
                            'email': objToMod.per_email,
                            'estado': objToMod.es_id
                        }
            context_aux['id']= objToMod.per_id
            form = forms.PersonasForm(initial=initialData)
            
        if form.is_valid()and request.POST.get('isForSave') is not None:
            if(request.POST.get('isForSave')=='true'): #Validacion extra para que no guarde si el campo esta vacio, pq al hacer por post
                # cree que debe mandar ya el formulario, y una vez que ya no manda, las validaciones de los campos impediran que se
                # mande sin completar
                obj = models.Personas()
                if form.data.get("id") is not None and form.data.get("id") != '':
                    obj = models.Personas.objects.filter(per_id=form.data.get("id"))
                    obj.update(per_nombre = form.data.get("nombre"),
                                per_apellido = form.data.get("apellido"),
                                per_cedula = form.data.get("documento"),
                                per_telefono = form.data.get("telefono"), 
                                per_email = form.data.get("email"),
                                es_id= form.data.get("estado")
                            )
                    context_aux['message']= 'Actualizacion Exitosa'
                else:
                    obj.es_id = form.data.get("estado")
                    obj.per_nombre = form.data.get("nombre")
                    obj.per_apellido = form.data.get("apellido")
                    obj.per_cedula = form.data.get("documento")
                    obj.per_telefono = form.data.get("telefono")
                    obj.per_email = form.data.get("email")
                    obj.usuario_alta = request.user.id
                    obj.save()
                    context_aux['message']= 'Insercion Exitosa'
                form = forms.PersonasForm(request.GET or None)
        if context is None:
            context = {
                "titulo": model,
                "da_form": form,
                "tabla_datos":models.Personas.objects.raw('''
                    SELECT 
                        p.per_id, 
                        e.es_desc,
                        p.per_nombre,
                        p.per_apellido,
                        p.per_cedula,
                        p.per_telefono,
                        p.per_email
                    FROM
                        personas p JOIN estados e ON (p.es_id = e.es_id)
                    ORDER BY
                        p.per_id desc
                ''')[:10],
                "tabla_titulos": models.Personas._meta.fields[:7],
                "menues": menues
            }
        return render(request, 'personas.html', (context | context_aux))
    if model == 'prioridades':
        context_aux = {}
        form = forms.PrioridadesForm(request.POST or None)
        if request.POST.get("delete") is not None and request.POST.get("delete") == "true":
            objToDelete = models.Prioridades.objects.filter(pri_id=request.POST.get("id"))
            objToDelete.delete()# A futuro ver si conviene realmente eliminar el registro
            context_aux['message']= 'Eliminacion Exitosa'

        if request.POST.get("edit") is not None and request.POST.get("edit") == "true":
            objToMod = get_object_or_404(models.Prioridades, pri_id=request.POST.get('id'))
            initialData = {'descripcion': objToMod.pri_desc, 
                            'nivel': objToMod.pri_nivel,
                            'estado': objToMod.es_id
                        }
            context_aux['id']= objToMod.pri_id
            form = forms.PrioridadesForm(initial=initialData)

        if form.is_valid()and request.POST.get('isForSave') is not None:
            if(request.POST.get('isForSave')=='true'): #Validacion extra para que no guarde si el campo esta vacio, pq al hacer por post
                # cree que debe mandar ya el formulario, y una vez que ya no manda, las validaciones de los campos impediran que se
                # mande sin completar
                obj = models.Prioridades()
                if form.data.get("id") is not None and form.data.get("id") != '':
                    obj = models.Prioridades.objects.filter(pri_id=form.data.get("id"))
                    obj.update(pri_desc = form.data.get("descripcion"),
                                pri_nivel = form.data.get("nivel"),
                                es_id= form.data.get("estado")
                            )
                    context_aux['message']= 'Actualizacion Exitosa'
                else:
                    obj.pri_desc = form.data.get("descripcion")
                    obj.pri_nivel = form.data.get("nivel")
                    obj.es_id = form.data.get("estado")
                    obj.usuario_alta = request.user.id
                    obj.save()
                    context_aux['message']= 'Insercion Exitosa'
                form = forms.PrioridadesForm(request.GET or None)
        if context is None:
            context = {
                "titulo": model,
                "da_form": form,
                "tabla_datos":models.Prioridades.objects.raw('''
                    SELECT 
                        p.pri_id ,
                        e.es_desc,
                        p.pri_desc,
                        p.pri_nivel
                    FROM 
                        prioridades p JOIN estados e ON ( p.es_id = e.es_id)
                    ORDER BY
                        p.pri_id desc
                ''')[:10],
                "tabla_titulos": models.Prioridades._meta.fields[:4],
                "menues": menues
            }
        return render(request, 'prioridades.html', (context | context_aux))
    if model == 'servicios':
        context_aux = {}
        form = forms.ServiciosForm(request.POST or None)
        if request.POST.get("delete") is not None and request.POST.get("delete") == "true":
            objToDelete = models.Servicios.objects.filter(ser_id=request.POST.get("id"))
            objToDelete.delete()# A futuro ver si conviene realmente eliminar el registro
            context_aux['message']= 'Eliminacion Exitosa'

        if request.POST.get("edit") is not None and request.POST.get("edit") == "true":
            objToMod = get_object_or_404(models.Servicios, ser_id=request.POST.get('id'))
            initialData = {'descripcion': objToMod.ser_desc, 
                            'estado': objToMod.es_id
                        }
            context_aux['id']= objToMod.ser_id
            form = forms.ServiciosForm(initial=initialData)

        if form.is_valid()and request.POST.get('isForSave') is not None:
            if(request.POST.get('isForSave')=='true'): #Validacion extra para que no guarde si el campo esta vacio, pq al hacer por post
                # cree que debe mandar ya el formulario, y una vez que ya no manda, las validaciones de los campos impediran que se
                # mande sin completar
                obj = models.Servicios()
                if form.data.get("id") is not None and form.data.get("id") != '':
                    obj = models.Servicios.objects.filter(ser_id=form.data.get("id"))
                    obj.update(ser_desc = form.data.get("descripcion"),
                                es_id= form.data.get("estado")
                            )
                    context_aux['message']= 'Edicion Exitosa'
                else: 
                    obj.es_id = form.data.get("estado")
                    obj.ser_desc = form.data.get("descripcion")
                    obj.usuario_alta = request.user.id
                    obj.save()
                    context_aux['message']= 'Insercion Exitosa'
                form = forms.ServiciosForm(request.GET or None)
        if context is None:
            context = {
                "titulo": model,
                "da_form": form,
                "tabla_datos":models.Servicios.objects.raw('''
                    SELECT 
                        s.ser_id ,
                        e.es_desc,
                        s.ser_desc
                    FROM 
                        servicios s JOIN estados e ON ( s.es_id = e.es_id)
                    ORDER BY
                        s.ser_id desc
                ''')[:10],
                "tabla_titulos": models.Servicios._meta.fields[:3],
                "menues": menues
            }
        return render(request, 'servicios.html', (context | context_aux))
    if model == 'terminales':
        context_aux = {}
        form = forms.TerminalesForm(request.POST or None)
        if request.POST.get("delete") is not None and request.POST.get("delete") == "true":
            objToDelete = models.Terminales.objects.filter(ter_id=request.POST.get("id"))
            objToDelete.delete()# A futuro ver si conviene realmente eliminar el registro
            context_aux['message']= 'Eliminacion Exitosa'

        if request.POST.get("edit") is not None and request.POST.get("edit") == "true":
            objToMod = get_object_or_404(models.Terminales, ter_id=request.POST.get('id'))
            initialData = {'servicio': objToMod.ser_id, 
                            'usuario': objToMod.usu_id,
                            'descripcion': objToMod.ter_desc,
                            'estado': objToMod.es_id
                        }
            context_aux['id']= objToMod.ter_id
            form = forms.TerminalesForm(initial=initialData)

        if form.is_valid()and request.POST.get('isForSave') is not None:
            if(request.POST.get('isForSave')=='true'): #Validacion extra para que no guarde si el campo esta vacio, pq al hacer por post
                # cree que debe mandar ya el formulario, y una vez que ya no manda, las validaciones de los campos impediran que se
                # mande sin completar
                obj = models.Terminales()
                if form.data.get("id") is not None and form.data.get("id") != '':
                    obj = models.Terminales.objects.filter(ser_id=form.data.get("id"))
                    obj.update(ser_id = form.data.get("servicio"),
                                usu_id= form.data.get("usuario"),
                                ter_desc= form.data.get("descripcion"),
                                es_id= form.data.get("estado"),
                                usuario_mod= request.user.id
                            )
                    context_aux['message']= 'Edicion Exitosa'
                else:
                    print("El estado", form.data.get("estado"))
                    obj.ser_id = form.data.get("servicio")
                    obj.usu_id= form.data.get("usuario")
                    obj.ter_desc= form.data.get("descripcion")
                    obj.es_id= form.data.get("estado")
                    obj.usuario_alta= request.user.id
                    
                    obj.save()
                    context_aux['message']= 'Insercion Exitosa'
                form = forms.TerminalesForm(request.GET or None)
        if context is None:
            context = {
                "titulo": model,
                "da_form": form,
                "tabla_datos":models.Terminales.objects.raw('''
                    SELECT                         
                        t.ter_id ,
                        e.es_desc ,
                        s.ser_desc ,
                        CASE WHEN (u.first_name IS NOT NULL AND u.first_name != '') OR (u.last_name IS NOT NULL AND u.last_name != '') THEN
                            u.first_name || u.last_name 
                        ELSE
                            u.username
                        END AS user ,
                        ter_desc
                    FROM
                        terminales t JOIN estados e ON (t.es_id = e.es_id)
                        JOIN servicios s ON (t.ser_id = s.ser_id)
                        JOIN auth_user u ON (t.usu_id = u.id)
                    ORDER BY 
                        t.ter_id desc
                ''')[:10],
                "tabla_titulos": models.Terminales._meta.fields[:5],
                "menues": menues
            }
        return render(request, 'terminales.html', (context | context_aux))
    if model == 'tickets':
        context_aux = {}
        form = forms.TicketsForm(request.POST or None)
        if request.POST.get("delete") is not None and request.POST.get("delete") == "true":
            objToDelete = models.Tickets.objects.filter(ticket_id=request.POST.get("id"))
            objToDelete.delete()# A futuro ver si conviene realmente eliminar el registro
            context_aux['message']= 'Eliminacion Exitosa'

        if request.POST.get("edit") is not None and request.POST.get("edit") == "true":
            objToMod = get_object_or_404(models.Tickets, ticket_id=request.POST.get('id'))
            initialData = {'descripcion': objToMod.ticket_desc, 
                            'inicio_numeracion': objToMod.ticket_inicio_num,
                            'fin_numeracion': objToMod.ticket_fin_num,
                            'estado': objToMod.es_id
                        }
            context_aux['id']= objToMod.ticket_id
            form = forms.TicketsForm(initial=initialData)

        if form.is_valid()and request.POST.get('isForSave') is not None:
            if(request.POST.get('isForSave')=='true'): #Validacion extra para que no guarde si el campo esta vacio, pq al hacer por post
                # cree que debe mandar ya el formulario, y una vez que ya no manda, las validaciones de los campos impediran que se
                # mande sin completar
                obj = models.Tickets()
                if form.data.get("id") is not None and form.data.get("id") != '':
                    obj = models.Tickets.objects.filter(ticket_id=form.data.get("id"))
                    #print("El objeto", obj.ticket_current_num)
                    #last_number = obj.ticket_current_num if obj.ticket_current_num is not None else 0
                    obj.update(ticket_desc = form.data.get("descripcion"),
                                ticket_inicio_num= form.data.get("inicio_numeracion"),
                                ticket_fin_num= form.data.get("fin_numeracion"),
                                #ticket_current_num= last_number+1,
                                es_id= form.data.get("estado"),
                                usuario_mod= request.user.id
                            )
                    context_aux['message']= 'Edicion Exitosa'
                else:
                    obj.ticket_desc = form.data.get("descripcion")
                    obj.ticket_inicio_num= form.data.get("inicio_numeracion")
                    obj.ticket_current_num = form.data.get("inicio_numeracion")
                    obj.ticket_fin_num= form.data.get("fin_numeracion")
                    obj.es_id= form.data.get("estado")
                    obj.usuario_alta= request.user.id
                    obj.save()
                    context_aux['message']= 'Insercion Exitosa'
                form = forms.TicketsForm(request.GET or None)
        if context is None:
            context = {
                "titulo": model,
                "da_form": form,
                "tabla_datos":models.Tickets.objects.raw('''
                    SELECT 
                        t.ticket_id,
                        e.es_desc,
                        t.ticket_desc,
                        t.ticket_inicio_num,
                        t.ticket_fin_num,
                        t.ticket_current_num
                    FROM
                        tickets t JOIN estados e ON (t.es_id = e.es_id)
                    ORDER BY 
                        t.ticket_id desc
                ''')[:10],
                "tabla_titulos": models.Tickets._meta.fields[:6],
                "menues": menues
            }
        return render(request, 'tickets.html', (context | context_aux))
    if model == 'tipocliente':
        context_aux = {}
        form = forms.TipoClienteForm(request.POST or None)
        if request.POST.get("delete") is not None and request.POST.get("delete") == "true":
            objToDelete = models.TipoCliente.objects.filter(tc_id=request.POST.get("id"))
            objToDelete.delete()# A futuro ver si conviene realmente eliminar el registro
            context_aux['message']= 'Eliminacion Exitosa'

        if request.POST.get("edit") is not None and request.POST.get("edit") == "true":
            objToMod = get_object_or_404(models.TipoCliente, tc_id=request.POST.get('id'))
            initialData = {'tipo_cliente': objToMod.tc_desc}
            context_aux['id']= objToMod.tc_id
            form = forms.TipoClienteForm(initial=initialData)

        if form.is_valid()and request.POST.get('isForSave') is not None:
            if(request.POST.get('isForSave')=='true'): #Validacion extra para que no guarde si el campo esta vacio, pq al hacer por post
                # cree que debe mandar ya el formulario, y una vez que ya no manda, las validaciones de los campos impediran que se
                # mande sin completar
                obj = models.TipoCliente()
                if form.data.get("id") is not None and form.data.get("id") != '':
                    obj = models.TipoCliente.objects.filter(tc_id=form.data.get("id"))
                    obj.update(tc_desc = form.data.get("tipo_cliente"))
                    context_aux['message']= 'Edicion Exitosa'
                else:
                    obj.tc_desc = form.data.get("tipo_cliente")
                    obj.save()
                    context_aux['message']= 'Insercion Exitosa'
                form = forms.TipoClienteForm(request.GET or None)
        if context is None:
            context = {
                "titulo": model,
                "da_form": form,
                "tabla_datos":models.TipoCliente.objects.all().order_by('tc_id')[:10],
                "tabla_titulos": models.TipoCliente._meta.fields[:7],
                "menues": menues
            }
        return render(request, 'tipocliente.html', (context | context_aux))
    if model == 'colasclientes' or model is None:
        context_aux = {}
        form = forms.ColasClientesForm(request.POST or None)
        if request.POST.get("cola_out") is not None and request.POST.get("cola_out") == "true":
            objToDelete = models.ColasClientes.objects.filter(id=request.POST.get("id"))
            estadoOut = models.Estados.objects.filter(es_desc='Salio de la Cola').first()
            if estadoOut is not None:
                objToDelete.update(es_id = estadoOut.es_id,
                                    cola_salida=datetime.datetime.now()
                                )# Eliminacion Logica
            else: 
                raise Exception("No existe el estado 'Salio de la Cola', no se puede proceder")
            context_aux['message']= 'Atencion Finalizada'

        if form.is_valid()and request.POST.get('isForSave') is not None:
            if(request.POST.get('isForSave')=='true'): #Validacion extra para que no guarde si el campo esta vacio, pq al hacer por post
                # cree que debe mandar ya el formulario, y una vez que ya no manda, las validaciones de los campos impediran que se
                # mande sin completar
                obj = models.ColasClientes()
                objCliente = models.Clientes.objects.filter(cli_ruc=form.data.get("cliente")).first()
                objPersona = models.Personas.objects.filter(per_cedula=form.data.get("cliente")).first()
                #Buscando por numero de cedula
                if objCliente is None and objPersona is not None:
                    objCliente = models.Clientes.objects.filter(per_id=objPersona.per_id).first()
                
                if objCliente is not None:# Ver como optimizar a futuro
                    obj.cli_id = objCliente.cli_id
                    obj.cola_id = form.data.get("cola")
                    obj.pri_id = form.data.get("prioridad")
                    estadoIn = models.Estados.objects.filter(es_desc='En Cola').first()
                    if estadoIn is not None:
                        obj.es_id = estadoIn.es_id
                    else:
                        raise Exception("No existe el estado 'En Cola', no se puede proceder")
                    #obj.es_id = estadoIn.es_id if estadoIn.es_id is not None else raise Exception("No existe el estado 'En Cola', no se puede proceder")
                    obj.save()
                    context_aux['message']= 'Cliente Añadido a la Cola'
                else:
                    context_aux['message']= 'Cliente No Existe'
                form = forms.ColasClientesForm(request.GET or None)
        if context is None:
            context = {
                "titulo": model if model is not None else 'colasclientes',
                "da_form": form,
                #Obtener todos los objetos de colaclientes, ordenadolos por nivel de prioridad (join con la tabla_datos
                # prioridad ), limitando a ver los primeros 20 registros
                "tabla_datos":models.ColasClientes.objects.raw('''
                    SELECT 
                        cc.id, 
                        c.cola_desc,
                        CASE WHEN cli.cli_razon_social IS NOT NULL AND cli.cli_razon_social != '' THEN
                            cli.cli_razon_social
                        ELSE 
                            per.per_nombre || ' ' || per.per_apellido
                        END as cli_desc,
                        p.pri_desc
                    FROM 
                        colas_clientes cc LEFT JOIN prioridades p ON (cc.pri_id = p.pri_id)
                        JOIN estados e ON (cc.es_id = e.es_id)
                        JOIN colas c ON (cc.cola_id = c.cola_id)
                        JOIN clientes cli ON (cc.cli_id = cli.cli_id)
                        JOIN personas per ON (cli.per_id = per.per_id)
                    WHERE
                        e.es_desc = 'En Cola'
                    ORDER BY 
                        p.pri_nivel asc, cc.cola_entrada asc
                ''')[:10],
                "tabla_titulos": models.ColasClientes._meta.fields[:4],
                "menues": menues
            }
        return render(request, 'colaclientes.html', (context | context_aux))

def saveOrUpdateData(request, form):
    print(request)

