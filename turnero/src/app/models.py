from django.db import models
from django.conf import settings


# Create your models here.
# los fields aparentemente tienen los siguientes parametros max_length, null, blank
class Estados(models.Model):
    es_id = models.AutoField(primary_key=True)
    es_desc = models.CharField(max_length=80, null=False, blank=False)
    es_cod = models.IntegerField(null=False, blank=False)

    class Meta:
        db_table = 'estados'

    def __str__(self) -> str:
        return self.es_desc


class Personas(models.Model):
    per_id = models.AutoField(primary_key=True)
    es = models.ForeignKey(Estados, on_delete=models.RESTRICT)
    per_nombre = models.CharField(max_length=100, null=False, blank=False)
    per_apellido = models.CharField(max_length=100, null=False, blank=False)
    per_cedula = models.CharField(max_length=10, null=True, blank=True)
    per_telefono = models.CharField(max_length=50, null=True, blank=True)
    per_email = models.EmailField(null=True, blank=True)
    fecha_alta = models.DateTimeField(auto_now_add=True)
    usuario_alta = models.IntegerField(null=False)
    fecha_mod = models.DateTimeField(auto_now=True)
    usuario_mod = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'personas'

    def __str__(self) -> str:
        return str(self.per_nombre + ' ' + self.per_apellido)


class TipoCliente(models.Model):
    tc_id = models.AutoField(primary_key=True)
    tc_desc = models.CharField(max_length=70, null=False, blank=False)

    class Meta:
        db_table = 'tipo_cliente'

    def __str__(self) -> str:
        return self.tc_desc


class Clientes(models.Model):
    cli_id = models.AutoField(primary_key=True)
    per = models.ForeignKey(Personas, on_delete=models.RESTRICT)
    tc = models.ForeignKey(TipoCliente, on_delete=models.RESTRICT)
    es = models.ForeignKey(Estados, on_delete=models.RESTRICT)
    cli_razon_social = models.CharField(max_length=100, null=True, blank=True)
    cli_ruc = models.CharField(max_length=15, null=True, blank=True)
    fecha_alta = models.DateTimeField(auto_now_add=True)
    usuario_alta = models.IntegerField(null=False)
    fecha_mod = models.DateTimeField(auto_now=True)
    usuario_mod = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'clientes'

    def __str__(self) -> str:
        return self.cli_razon_social


class Prioridades(models.Model):
    pri_id = models.AutoField(primary_key=True)
    es = models.ForeignKey(Estados, on_delete=models.RESTRICT)
    pri_desc = models.CharField(max_length=100, null=False, blank=False)
    pri_nivel = models.IntegerField(null=False, blank=False, default=0)
    fecha_alta = models.DateTimeField(auto_now_add=True)
    usuario_alta = models.IntegerField(null=False)
    fecha_mod = models.DateTimeField(auto_now=True)
    usuario_mod = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'prioridades'

    def __str__(self) -> str:
        return self.pri_desc


class Tickets(models.Model):
    ticket_id = models.AutoField(primary_key=True)
    es = models.ForeignKey(Estados, on_delete=models.RESTRICT)
    ticket_desc = models.CharField(max_length=250, null=False, blank=False)
    ticket_inicio_num = models.IntegerField(null=False)
    ticket_fin_num = models.IntegerField(null=False)
    ticket_current_num = models.IntegerField(null=False)
    fecha_alta = models.DateTimeField(auto_now_add=True)
    usuario_alta = models.IntegerField(null=False)
    fecha_mod = models.DateTimeField(auto_now=True)
    usuario_mod = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'tickets'

    def __str__(self) -> str:
        return self.ticket_desc


class Servicios(models.Model):
    ser_id = models.AutoField(primary_key=True)
    es = models.ForeignKey(Estados, on_delete=models.RESTRICT)
    ser_desc = models.CharField(max_length=100, null=False, blank=False)
    fecha_alta = models.DateTimeField(auto_now_add=True)
    usuario_alta = models.IntegerField(null=False)
    fecha_mod = models.DateTimeField(auto_now=True)
    usuario_mod = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'servicios'

    def __str__(self) -> str:
        return self.ser_desc


# Hacer un alter table para poder enlazar la tabla auth user con
# la tabla terminales
# ALTER TABLE app_terminales
# ADD FOREIGN KEY (usuario_id) REFERENCES auth_user(id);
class Terminales(models.Model):
    ter_id = models.AutoField(primary_key=True)
    es = models.ForeignKey(Estados, on_delete=models.RESTRICT)
    ser = models.ForeignKey(Servicios, on_delete=models.RESTRICT)
    usu = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.RESTRICT,)
    ter_desc = models.CharField(max_length=100, null=False, blank=False)
    fecha_alta = models.DateTimeField(auto_now_add=True)
    usuario_alta = models.IntegerField(null=False)
    fecha_mod = models.DateTimeField(auto_now=True)
    usuario_mod = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'terminales'

    def __str__(self) -> str:
        return self.ter_desc


class Colas(models.Model):
    cola_id = models.AutoField(primary_key=True)
    ticket = models.ForeignKey(Tickets, on_delete=models.RESTRICT)
    es = models.ForeignKey(Estados, on_delete=models.RESTRICT)
    ter = models.ForeignKey(Terminales, on_delete=models.RESTRICT)
    cola_desc = models.CharField(max_length=150, null=False, blank=False)
    cola_max_persona = models.IntegerField()
    cola_current_persona = models.IntegerField()
    cola_increment = models.IntegerField()

    class Meta:
        db_table = 'colas'

    def __str__(self) -> str:
        return self.cola_desc


class ColasClientes(models.Model):
    cola = models.ForeignKey(Colas, on_delete=models.RESTRICT)
    cli = models.ForeignKey(Clientes, on_delete=models.RESTRICT)
    pri = models.ForeignKey(Prioridades, on_delete=models.RESTRICT, null=True, blank=True)
    es = models.ForeignKey(Estados, on_delete=models.RESTRICT)
    cola_entrada = models.DateTimeField(auto_now=True)
    cola_salida = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'colas_clientes'

    def __str__(self) -> str:
        return self.cola.cola_desc
